//
// Created by LiaoJiaPeng on 19-11-28.
//

#include "BoxSystem.h"
#include <chrono>

bool BoxSystem::_quitFlag;

BoxSystem::BoxSystem(std::string cameraName,std::string portName):_Bcamera(cameraName),
                                                                  _serial(portName),
                                                                  _imageBuffer(1),
                                                                  _serialOpenFlag(false)
{
    _feedbackData.xv = 0;
    _feedbackData.yv = 0;
    _feedbackData.time = 0;
}

BoxSystem::~BoxSystem()
{
}

bool BoxSystem::init()
{
    if(!_Bcamera.open() || !_serial.open())
    {
        return false;
    }
    _serialOpenFlag = true;
    return true;
}

void BoxSystem::producer()
{
    VideoCapture cap(0);
    Mat frame;

    for(;;)
    {
        if(_quitFlag)
        {
            return;
        }
        cap>>frame;
        _imageBuffer.push(frame.clone());
        cout<<"producer"<<endl;
    }
}

void BoxSystem::consumer()
{

    vector<float> coordination;
    int argc = 0; char **argv = nullptr;
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    _Bdetector.w_ = &w;
    for(;;)
    {
    if(_quitFlag)
    {
        return;
    }
    cv::Mat img;
    _imageBuffer.pop(img);
    int count;//what? qt?


    //auto start = chrono::system_clock::now();
    //imshow("img",img);

    if(img.empty())
    {
            cout<<"img is empty"<<endl;
            continue;
    }
    else
    {

        coordination = _Bdetector.autoAlignment(img);
        _Bdetector.showBoxImage(img, false);
        _saveframe.saveframe(img);
        waitKey(1);
    }
    cout<<"consumer"<<endl;
    }

}

void BoxSystem::signalHandler(int)
{
    _quitFlag = true;
}

