//
// Created by LiaoJiaPeng on 19-12-5.
//

#ifndef ENGINEERPRO_ARMORDETECTIONSYSTEM_H
#define ENGINEERPRO_ARMORDETECTIONSYSTEM_H
#include "../common/CycleQueue.h"
#include "../common/Logger.h"
#include "../driver/SerialPort.h"
#include "../driver/BoxCamera.h"
#include "../perception/ArmorDetection.h"
#include <signal.h>

class ArmorDetectionSystem {
public:
    ArmorDetectionSystem(std::string cameraName,std::string portName);

    ~ArmorDetectionSystem();

    void Aproducer();

    void Aconsumer();

private:

    SerialPort            _serial;
    BoxCamera             _Bcamera;    //boxcamera
    CycleQueue<cv::Mat>   _imageBufferA;
    ArmorDetection        _Adetector;
    static bool           _quitFlag;


};


#endif //ENGINEERPRO_ARMORDETECTIONSYSTEM_H
