//
// Created by dl1ja12 on 19-11-28.
//

#ifndef ENGINEERPRO_CALCULATEFORBOX_H
#define ENGINEERPRO_CALCULATEFORBOX_H
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;


class CalculateForBox {
public:
    CalculateForBox();

    ~CalculateForBox();

    double getDistance(Point pointO, Point pointA);

    Vec3f getMeans(std::vector<Vec3f> cricles);

    float getK(Vec4i line);

    Point2f cpmputeSetCenter(vector<Point2f> pointSet);


};


#endif //ENGINEERPRO_CALCULATEFORBOX_H
