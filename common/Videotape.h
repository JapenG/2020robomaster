//
// Created by dl1ja12 on 20-1-12.
//

#ifndef ENGINEERPRO_VIDEOTAPE_H
#define ENGINEERPRO_VIDEOTAPE_H

#include <iostream>
#include <assert.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <fstream>
#include <thread>
#include <mutex>
#include <map>

class SaveFrame {
public:
    SaveFrame(std::string FilePath = "/home/dl1ja12/2020robomaster/video/",
              cv::Size sWH = cv::Size(640,360),
              float frameRate = 330.0);

    ~SaveFrame();

    bool saveframe(cv::Mat frame);

public:
    std::string getFileName();

    std::string fileName = "";

    cv::VideoWriter capsaveVideo;



};



#endif //ENGINEERPRO_VIDEOTAPE_H
