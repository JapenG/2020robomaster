//
// Created by dl1ja12 on 20-1-12.
//

#include "Videotape.h"


SaveFrame::SaveFrame(std::string FilePath, cv::Size sWH, float frameRate)
{
    fileName = getFileName() + ".avi";
    capsaveVideo.open(FilePath + fileName,
                      cv::VideoWriter::fourcc('M', 'P', '4', '2'),
                      frameRate,
                      sWH);
}

SaveFrame::~SaveFrame()
{

    std::cout<<"111"<<std::endl;
    capsaveVideo.release();

}

bool SaveFrame::saveframe(cv::Mat frame)
{
    capsaveVideo.write(frame);
    return true;
}

std::string SaveFrame::getFileName()
{
    time_t currentTime;
    struct tm* timePointer;
    time(&currentTime);
    timePointer = localtime(&currentTime);

    return std::to_string(timePointer->tm_year + 1900) + "-" +
           std::to_string(timePointer->tm_mon + 1) + "-" +
           std::to_string(timePointer->tm_mday) + "_" +
           std::to_string(timePointer->tm_hour) + ":" +
           std::to_string(timePointer->tm_min) + ":" +
           std::to_string(timePointer->tm_sec);
}