//
// Created by LiaoJiaPeng on 19-11-28.
//

#ifndef ENGINEERPRO_ARMORDETECTION_H
#define ENGINEERPRO_ARMORDETECTION_H
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
constexpr auto TEAMBLUE = 0;
constexpr auto TEAMRED = 1;


class Armor{
public:
    cv::RotatedRect armorS;
    int builds1_No = 0;
    int builds2_No = 0;
    int build_features[4];
};

class ArmorDetection{
public:
    ArmorDetection();

    ~ArmorDetection();

    void autoShoot();

    void findArmor(cv::Mat& src, cv::Mat& dst, std::vector<cv::RotatedRect>& all, cv::RotatedRect& target);

    void imgPreprocess(const cv::Mat& src, cv::Mat& dst);

public:
    cv::Mat armorImg;
    cv::Mat preImg;
    int lightColor;
    std::vector<cv::RotatedRect> allTarget;
    cv::RotatedRect aimTarget;

};


#endif //ENGINEERPRO_ARMORDETECTION_H
