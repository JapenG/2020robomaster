# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/dl1ja12/2020robomaster/cmake-build-debug/EngineerPro_autogen/mocs_compilation.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/EngineerPro_autogen/mocs_compilation.cpp.o"
  "/home/dl1ja12/2020robomaster/common/CalculateForBox.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/common/CalculateForBox.cpp.o"
  "/home/dl1ja12/2020robomaster/common/Logger.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/common/Logger.cpp.o"
  "/home/dl1ja12/2020robomaster/common/Videotape.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/common/Videotape.cpp.o"
  "/home/dl1ja12/2020robomaster/common/mainwindow.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/common/mainwindow.cpp.o"
  "/home/dl1ja12/2020robomaster/common/qcustomplot.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/common/qcustomplot.cpp.o"
  "/home/dl1ja12/2020robomaster/driver/BoxCamera.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/driver/BoxCamera.cpp.o"
  "/home/dl1ja12/2020robomaster/driver/SerialPort.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/driver/SerialPort.cpp.o"
  "/home/dl1ja12/2020robomaster/main.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/main.cpp.o"
  "/home/dl1ja12/2020robomaster/perception/ArmorDetection.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/perception/ArmorDetection.cpp.o"
  "/home/dl1ja12/2020robomaster/perception/BoxDetection.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/perception/BoxDetection.cpp.o"
  "/home/dl1ja12/2020robomaster/system/ArmorDetectionSystem.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/system/ArmorDetectionSystem.cpp.o"
  "/home/dl1ja12/2020robomaster/system/BoxSystem.cpp" "/home/dl1ja12/2020robomaster/cmake-build-debug/CMakeFiles/EngineerPro.dir/system/BoxSystem.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_PRINTSUPPORT_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "EngineerPro_autogen/include"
  "/usr/local/include/opencv4"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64"
  "/usr/include/x86_64-linux-gnu/qt5/QtPrintSupport"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
