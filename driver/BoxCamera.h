//
// Created by dl1ja12 on 19-12-4.
//

#ifndef ENGINEERPRO_BOXCAMERA_H
#define ENGINEERPRO_BOXCAMERA_H


#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include "../perception/ArmorDetection.h"

class BoxCamera {
public:
    BoxCamera(std::string cameraName, int imageWidth = 640, int imageHeight = 480);

    ~BoxCamera();

    bool open();

    bool CameraRead(ArmorDetection& Aparam);

    bool getFrame(cv::Mat& img);

    void restart();


private:
    cv::VideoCapture  cam;
    cv::Mat           frame;
    std::string     _cameraName;
    int             _cameraNums;
    int             _imageWidth;
    int             _imageHeight;





};


#endif //ENGINEERPRO_BOXCAMERA_H
