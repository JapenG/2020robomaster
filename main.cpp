#include <iostream>
#include <thread>
#include "system/BoxSystem.h"
#include "system/ArmorDetectionSystem.h"

int main()
{

    //ArmorDetectionSystem armorDetectionSystem("ea");
    BoxSystem boxSystem("Engineer","");
    //boxSystem.init();
    //std::thread Aproducer(&ArmorDetectionSystem::Aproducer, std::ref(armorDetectionSystem));
    //std::thread Aconsumer(&ArmorDetectionSystem::Aconsumer, std::ref(armorDetectionSystem));
    std::thread producer(&BoxSystem::producer, std::ref(boxSystem));
    std::thread consumer(&BoxSystem::consumer, std::ref(boxSystem));
    //Aproducer.join();
    producer.join();

    consumer.join();
    //Aconsumer.join();


    return 0;
}